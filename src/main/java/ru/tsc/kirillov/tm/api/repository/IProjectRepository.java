package ru.tsc.kirillov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@Nullable String userId, @NotNull String name);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    String[] findAllId(@Nullable String userId);

}
