package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.command.project.*;
import ru.tsc.kirillov.tm.command.system.*;
import ru.tsc.kirillov.tm.command.task.*;
import ru.tsc.kirillov.tm.command.user.*;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.kirillov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.kirillov.tm.model.Project;
import ru.tsc.kirillov.tm.model.User;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.util.DateUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService =  new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationAllCommandCommand());
        registry(new ApplicationAllArgumentCommand());

        registry(new TaskCreateCommand());
        registry(new TaskListCommand());
        registry(new TaskClearCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskCompletedByIdCommand());
        registry(new TaskCompletedByIndexCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectCompletedByIdCommand());
        registry(new ProjectCompletedByIndexCommand());
        registry(new ProjectBindTaskByIdCommand());
        registry(new ProjectUnbindTaskByIdCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** Task Manager завершил свою работу **");
            }
        });
    }

    private void initData() {
        @Nullable final User user = userService.findByLogin("test");
        @NotNull String userId = "";
        if (user != null)
            userId = user.getId();
        taskService.create(userId, "Тестовая задача", "Простая задача");
        taskService.create(userId,"Вторая задача", "Простая задача");
        taskService.create(userId,"Ещё одна задача", "Простая задача");

        projectService.create(userId,"Тестовый проект", "Простой проект");
        projectService.create(userId,"Второй проект", "Простой проект");
        projectService.create(userId,"Ещё один проект", "Простой проект");
        projectService.add(
                userId,
                new Project(
                        "Тест с датой",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("04.10.2019")
                )
        );
        projectService.add(
                userId,
                new Project(
                        "Не запущенный тестовый проект",
                        Status.NOT_STARTED,
                        DateUtil.toDate("05.03.2018")
                )
        );
        projectService.add(
                userId,
                new Project(
                        "Выполняющийся проект",
                        Status.IN_PROGRESS,
                        DateUtil.toDate("16.02.2020")
                )
        );
        projectService.add(
                userId,
                new Project(
                        "Завершённый проект",
                        Status.COMPLETED,
                        DateUtil.toDate("22.01.2021")
                )
        );
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0)
            return false;

        @Nullable final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null)
            throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void close() {
        System.exit(0);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args))
            close();

        initLogger();
        initUsers();
        initData();

        while (true) {
            try {
                System.out.println("\nВведите команду:");
                @NotNull final String cmdText = TerminalUtil.nextLine();
                processCommand(cmdText);
                System.out.println("[Ок]");
                loggerService.command(cmdText);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[Ошибка]");
            }
        }
    }

}
