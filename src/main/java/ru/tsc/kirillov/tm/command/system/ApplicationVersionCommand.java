package ru.tsc.kirillov.tm.command.system;

import org.jetbrains.annotations.NotNull;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отображение версии программы.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.23.0");
    }

}
